<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ultimate Dance Legacy is a multi-faceted dance studio Located in Huntington, WV. We offer classes in ballet, acro, tap, jazz, lyrical/contemporary, musical theatre and pointe.">
    <meta name="author" content="Alex Hackney, Kindred Digital">
    <title>Ultimate Dance Legacy | Huntington, WV</title>
    <link rel="canonical" href="https://ultimatedancelegacy.com">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
    <link href="/styles/styles.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="/">Ultimate Dance Legacy!</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/about">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/classes">Classes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/policies">Studio Policies &amp; Dress Code</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/summer-camps">Summer Camps</a>
          </li>
        </ul>
          <a href="https://app.jackrabbitclass.com/jr3.0/ParentPortal/Login?orgID=540311" target="_blank" class="btn bt-lg btn-primary parent">Parent Portal</a>
          <a href="https://app.jackrabbitclass.com/regv2.asp?id=540311" target="_blank" class="btn bt-lg btn-primary enroll">Enroll Now</a>
      </div>
    </nav>
    <main role="main">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="hero owl-carousel owl-theme">
              <img class="owl-lazy" data-src="/img/frontpage/slider-1.jpg" data-src-retina="/img/frontpage/slider-1-retina.jpg" alt="Ultimate Dance Legacy">
              <img class="owl-lazy" data-src="/img/frontpage/slider-2.jpg" data-src-retina="/img/frontpage/slider-2-retina.jpg" alt="Ultimate Dance Legacy">
              <img class="owl-lazy" data-src="/img/frontpage/slider-3.jpg" data-src-retina="/img/frontpage/slider-3-retina.jpg" alt="Ultimate Dance Legacy">
              <img class="owl-lazy" data-src="/img/frontpage/slider-4.jpg" data-src-retina="/img/frontpage/slider-4-retina.jpg" alt="Ultimate Dance Legacy">
              <img class="owl-lazy" data-src="/img/frontpage/slider-5.jpg" data-src-retina="/img/frontpage/slider-5-retina.jpg" alt="Ultimate Dance Legacy">
              <img class="owl-lazy" data-src="/img/frontpage/slider-6.jpg" data-src-retina="/img/frontpage/slider-6-retina.jpg" alt="Ultimate Dance Legacy">
              <img class="owl-lazy" data-src="/img/frontpage/slider-7.jpg" data-src-retina="/img/frontpage/slider-7-retina.jpg" alt="Ultimate Dance Legacy">
              <img class="owl-lazy" data-src="/img/frontpage/slider-8.jpg" data-src-retina="/img/frontpage/slider-8-retina.jpg" alt="Ultimate Dance Legacy">
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h1>Ultimate Dance Legacy</h1>
            <p>5736 E Pea Ridge Rd #203<br/>
            Huntington, WV 25705</p>
            <p>Phone Number: <a href="tel:13047332623">(304) 733-2623</a></p>
            <p>E-mail: <a href="mailto:ultimatedancelegacy@gmail.com">ultimatedancelegacy@gmail.com</a></p>
          </div>
        </div>
        <hr/>
        <div class="row">
          <div class="col-md-12 text-center">
            <h2>Follow Us on Instagram!</h2>
            <p class="instagram-loading blinking">Loading Instagram...</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <div id="instagram" class="instagram owl-carousel owl-theme">
            </div>
          </div>
        </div>
      </div>
    </main>
    <footer class="container-fluid">
      <p><a href="http://ultimatedancelegacy.com">&copy; UltimateDanceLegacy.com</a></p>
      <p><a href="https://www.facebook.com/ultimatedancelegacy/"><img src="/img/facebook-logo.jpg" class="social"></a><a href="https://www.instagram.com/ultimatedancelegacy/"><img src="/img/instagram-logo.jpg" class="social"></a></p>
    </footer>
    <script src="/js/app.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-148179500-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-148179500-1');
    </script>
    <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script', 'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '328328034391743');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=328328034391743&ev=PageView&noscript=1"/></noscript>
  </body>
</html>
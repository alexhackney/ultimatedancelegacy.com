<?php
require __DIR__ . '/../vendor/autoload.php';
// If account is public you can query Instagram without auth

// Let's look at $media
// $media = $medias[2];
// echo "Media info:\n";
// echo "Id: {$media->getId()}\n";
// echo "Shortcode: {$media->getShortCode()}\n";
// echo "Created at: {$media->getCreatedTime()}\n";
// echo "Caption: {$media->getCaption()}\n";
// echo "Number of comments: {$media->getCommentsCount()}";
// echo "Number of likes: {$media->getLikesCount()}";
// echo "Get link: {$media->getLink()}";
// echo "High resolution image: {$media->getImageHighResolutionUrl()}";

// echo "Media type (video or image): {$media->getType()}";
// $account = $media->getOwner();
// echo "Account info:\n";
// echo "Id: {$account->getId()}\n";
// echo "Username: {$account->getUsername()}\n";
// echo "Full name: {$account->getFullName()}\n";
// echo "Profile pic url: {$account->getProfilePicUrl()}\n";

$slidesFile = "instagram/current-slides.json";

return returnSlides($slidesFile);


function downloadSlides($slides, $slidesFile)
{
    foreach ($slides as &$slide) {
        $fileNameParts = explode('/', explode('?', $slide['url'])[0]);
        $fileName = end($fileNameParts);
        $path = 'instagram/'. $fileName;
        file_put_contents($path, fopen($slide['url'], 'r'));
        $slide['url'] = "/$path";
    }
    writeFile($slides, $slidesFile);
}


function getNewSlides($slidesFile)
{
    $instagram = new \InstagramScraper\Instagram();
    $medias = $instagram->getMedias('ultimatedancelegacy', 10);

    $slides = array();
    foreach ($medias as $media) {
        if ($media->getType() != 'sdfsdfsd') {
            $slide = array(
                'url' => $media->getImageHighResolutionUrl(),
                'caption' => $media->getCaption(),
                'link' => $media->getLink()
            );
        
            array_push($slides, $slide);
        }
    }

    downloadSlides($slides, $slidesFile);
}

function returnSlides($slidesFile)
{
    $timeStamp = filemtime($slidesFile);
  
    $difference = time()-$timeStamp;

    if ($difference > 3600) {
        var_dump('reading new files');
        getNewSlides($slidesFile);
    }

    return readfile($slidesFile);
}

function writeFile($slides, $slidesFile)
{
    $instagramFile = fopen($slidesFile, "w")
    or die(json_encode(['error' => 'Unable to open file!']));
    
    fwrite($instagramFile, json_encode($slides));

    fclose($instagramFile);
}

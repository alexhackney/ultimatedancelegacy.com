$('.hero').owlCarousel({
    loop:true,
    autoplay:true,
    lazyLoad:true,
    margin:0,
    padding:0,
    nav:false,
    responsive:{
        0:{
            items:1
        }
    }
});

    //Instagram Slider
    function getSlides(){
        var slides;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var data = this.responseText;
                var slides = JSON.parse(data);
                for (var i = 0; i < slides.length; i++) {
                    var obj = slides[i];
                    $('#instagram').owlCarousel().trigger('add.owl.carousel', [jQuery('<a href="' + slides[i].link + '" target="_blank"><img class="item" src="' + slides[i].url + '" alt="' + slides[i].caption + '" title="' + slides[i].caption + '"></a>')]).trigger('refresh.owl.carousel');
                }
            }
        };
      
      xhttp.open("GET", "/instagram.php", true);
      xhttp.send();
        
    }

getSlides();

$('.instagram').owlCarousel({
            loop:true,
            autoplay:true,
            lazyLoad:true,
            margin:10,
            autoHeight:true,
            padding:10,
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:3
                },
                1024:{
                    items:5
                }
            }
        }).on('changed.owl.carousel', function(event) {
            var elem = document.querySelector('.instagram-loading');
            elem.style.display = 'none';
        });
    
    


